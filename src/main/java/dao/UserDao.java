/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author PC
 */
public class UserDao implements DaoTnterface<User> {

    @Override
    public int add(User object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO USER(USER_NAME,USER_TEL,USER_PASSWORD)VALUES (?,?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT USER_ID,USER_NAME,USER_TEL,USER_PASSWORD FROM USER";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("USER_ID");
                String name = result.getString("USER_NAME");
                String tel = result.getString("USER_TEL");
                String pass = result.getString("USER_PASSWORD");
                User user = new User(id, name, tel,pass);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT USER_ID,USER_NAME,USER_TEL,USER_PASSWORD FROM USER WHERE USER_ID =" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("USER_ID");
                String name = result.getString("USER_NAME");
                String tel = result.getString("USER_TEL");
                String pass = result.getString("USER_PASSWORD");
                User user = new User(pid, name, tel,pass);
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM USER WHERE USER_ID = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE USER SET USER_NAME = ?,USER_TEL = ? WHERE USER_ID = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User(-1, "Pannatad Juntanan", "0855555555","pass"));
        System.out.println("id: " + id);
        System.out.println(dao.get(id));
        User lastUser = dao.get(id);
        System.out.println("lsat user: " + lastUser);
        lastUser.setPassword("pass1");
        int row = dao.update(lastUser);
        User updateUser = dao.get(id);
        System.out.println("update user: " + updateUser);
        dao.delete(id);
    }
}
