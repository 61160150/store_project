/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Customer;

/**
 *
 * @author PC
 */
public class CustomerDao implements DaoTnterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO CUSTOMER(CUS_NAME,CUS_TEL)VALUES (?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT CUS_ID,CUS_NAME,CUS_TEL FROM CUSTOMER";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("CUS_ID");
                String name = result.getString("CUS_NAME");
                String tel = result.getString("CUS_TEL");
                Customer customer = new Customer(id, name, tel);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT CUS_ID,CUS_NAME,CUS_TEL FROM CUSTOMER WHERE CUS_ID =" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("CUS_ID");
                String name = result.getString("CUS_NAME");
                String tel = result.getString("CUS_TEL");
                Customer customer = new Customer(pid, name, tel);
                return customer;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM CUSTOMER WHERE CUS_ID = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE CUSTOMER SET CUS_NAME = ?,CUS_TEL = ? WHERE CUS_ID = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "Pannatad Juntanan", "0855555555"));
        System.out.println("id: " + id);
        System.out.println(dao.get(id));
        Customer lastCustomer = dao.get(id);
        System.out.println("lsat customer: " + lastCustomer);
        lastCustomer.setTel("0866666666");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("update customer: " + updateCustomer);
        dao.delete(id);
    }
}
